---
layout: page
title: About
permalink: /about/
---

My name is Richie Thomas, and I'm a former travel agent turned English teacher turned software engineer.  Since I don't have the typical background of a 4-year computer science degree, I'm leveling up on my own.  One of the ways I'm doing this is by reading other peoples' code.  Hence this blog, where I'll document my progress and hold myself accountable.

Gitlab is a company I admire, both for its technical competence and its remote-only ethos.  One of my career goals is to work there someday.  I figure that by documenting my progress at learning the codebase, I'll have a better chance of reaching that goal.  Failing that, by maintaining this blog, I'll be more likely to retain the things I learn for future use.  Lastly (and perhaps most importantly), I'm a big believer in "sending the elevator back down" (i.e. helping people who ).  In that spirit, I hope the things I write here may be useful to others!