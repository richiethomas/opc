---
layout: post
author: Richie Thomas
title: What happens when I type "gdk run"? Introduction
subtitle: What to expect from this series of posts
---

## Introduction

Welcome to this series of posts on how `gdk run` works!  I'll walk through what happens when you type this command into your terminal, in as much detail as possible.  Sometimes while walking through a file that contains `run`-related code, I'll take detours to explore other nearby branches of code.  I'll try to call those out when they happen, and provide links so you can skip those tangents if you'd like.

I start by talking about what happens specifically on *my* machine, since I have a Ruby version manager installed and the initial `gdk run` command is intercepted by that version manager's shim.  If that's not interesting to you or you don't have a similar setup on your locall, feel free to skip to Part 1.  Although you [probably should install](https://thoughtbot.com/blog/psa-do-not-use-system-ruby) a Ruby version manager.  Just sayin'. 😉

## Caution

The posts in this series represent one person's evolving knowledge of how the Gitlab works at one point in time.  The codebase will change faster than I (and therefore these posts) can keep up with it.  They are not to be taken as documentation.  Contributions and corrections are appreciated.

## Table Of Contents

 - Part 0- [What happens when an RBENV shim runs?](/opc/what-happens-when-I-type-gdk-run-part-0)
 - Part 1- [The Gitlab Development Kit CLI launcher](/opc/what-happens-when-I-type-gdk-run-part-1)
 - Part 2- [Defining the bulk of `gdk` commands](/opc/what-happens-when-I-type-gdk-run-part-2)
 - Part 3- [Setting hosts, posts, and root_dirs for the `run` command](/opc/what-happens-when-I-type-gdk-run-part-3)
 - Part 4- [Deciding which Gitlab services to run](/opc/what-happens-when-I-type-gdk-run-part-4)
 - Part 5- [Daemonizing the `gdk run` process](/opc/what-happens-when-I-type-gdk-run-part-5)

Ready?  Let's gooooo!

Starting with [Part 0- Ruby shims](/opc/what-happens-when-I-type-gdk-run-part-0).