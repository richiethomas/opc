---
layout: post
author: Richie Thomas
title: What happens when I type "gdk run"? Part 3
subtitle: Setting hosts, posts, and root_dirs for the `run` command
---

In [Part 2 of this guide](/opc/what-happens-when-I-type-gdk-run-part-2/), we talked about the `GDK` Ruby module, and how it executes a case statement that checks which `gdk` command the user entered.  If you recall, one of the branches of that case statement looks like the following:

```
  32:     when 'run'
  33:       exec('./run', *ARGV, chdir: $gdk_root)
```
This means that if the user enters `gdk run`, the Ruby interpreter executes the `run` bash script from the project's root directory, passing along any arguments to this script.  In this post, we'll talk about what happens inside this bash script.

---

<details>

<summary>Click here to see code in full</summary>

```
#!/bin/sh

# TODO rewrite config parsing in lib/run.rb

main() {
  # Try to read the gitlab-workhorse host:port from the environments
  if [ "x${host}" = x ]; then
    # Try a host/hostname file
    host="$(cat host 2>/dev/null || cat hostname 2>/dev/null)"
  fi
  if [ "x${port}" = x ]; then
    # Try a port file
    port="$(cat port 2>/dev/null)"
  fi
  if [ "x${relative_url_root}" = x ]; then
    # Try a relative_url_root file
    relative_url_root="$(cat relative_url_root 2>/dev/null)"
  fi

  # Fall back to 0.0.0.0:3000
  host="${host:-0.0.0.0}"
  port="${port:-3000}"
  relative_url_root="${relative_url_root:-/}"

  export host
  export port
  export relative_url_root

  exec ruby lib/run.rb "$@"
}

if [ "x$GDK_RUNIT" = "x1" ]; then
  echo "$0 is disabled while using runit"
  exit 1
fi

main "$@"

```

</details>

---

## Checking for the "$GDK_UNIT" envar

  ```
  if [ "x$GDK_RUNIT" = "x1" ]; then
    echo "$0 is disabled while using runit"
    exit 1
  fi

  main "$@"
  ```

I'm actually starting with the end of the file this time, because the only other code in this file is the definition of the `main` function.

This "if" conditional checks to see if the `$GDK_RUNIT` envar is set to `1`, and if so, prints an error message to the console and exits with a non-zero return code.  If not, the `main` function is called, passing to it any arguments which were passed to the bash script itself.

One thing I was confused about is why the above syntax looks the way it does (i.e. `if [ "x$GDK_RUNIT" = "x1" ]`), instead of the following:

```
if [ "$GDK_RUNIT" = "1" ]
```

In other words, why are both sides of the conditional prefixed with `x`?  [I posted a question about this on StackOverflow](https://stackoverflow.com/questions/57649857/difference-between-if-xfoo-x1-vs-if-foo-1-in-bash), and ~~am waiting for an answer~~ got [this answer](https://stackoverflow.com/a/57649879/2143275) and [this answer](https://stackoverflow.com/questions/174119/why-do-shell-script-comparisons-often-use-xvar-xyes).  TL;DR- the `x` prefix is a method of preventing errors from being raised if `$GDK_RUNIT` is blank, or if it's set to a value that bash interprets to be a flag (ex.- `-n` or something).

---

## The `main` method

Now for the bulk of the file.

### Checking for `host`

```
if [ "x${host}" = x ]; then
  # Try a host/hostname file
  host="$(cat host 2>/dev/null || cat hostname 2>/dev/null)"
fi
```

The first line in the `if` block checks to see if the value of a variable named `host` is nil.  It does this using a bash script convention of joining an `x` with the string-interpolated `host` variable.  If this conjoined string is equal to `x`, then `host` must be nil.  We talked about this Bash convention above.

If `host` is in fact nil, then the script sets its value equal to the output of `cat host 2>/dev/null || cat hostname 2>/dev/null`.  As the comment above the line suggests, this line first tries to read the contents of a file named `host`, and if that doesn't work, tries again with a filename of `hostname`.  In either case, if there is a file with this name, the `host` variable inside this script will be set equal to the contents of that file.  If neither file exists, this variable will continue to be null.

---

### Checking for `port`

```
if [ "x${port}" = x ]; then
  # Try a port file
  port="$(cat port 2>/dev/null)"
fi
```

This `if` block does basically the same thing, but with the variable `port`.  If this variable hasn't been previously declared via exporting to an envar, then the script checks for a file named `port` and sets the local variable named `port` equal to the contents of that file if that file is found.  Otherwise, the variable continues to be nil.

---

### Checking for `relative_url_root`

```
if [ "x${relative_url_root}" = x ]; then
  # Try a relative_url_root file
  relative_url_root="$(cat relative_url_root 2>/dev/null)"
fi
```

Exact same logic here, but with an envar/local variable named `relative_url_root`.

---

### Setting intelligent defaults

```
# Fall back to localhost:3000
host="${host:-localhost}"
port="${port:-3000}"
relative_url_root="${relative_url_root:-/}"
```

[The `:-` syntax](https://devhints.io/bash) says that a variable is either equal to its previous value, or (if the previous value is nil) the value specified after `:-`.  For example, `host="${host:-localhost}"` says that the new value of `host` is equal to either the old value of `host`, or "localhost".  Same logic applies to `port` and `relative_url_root`.

This is a bit like the `||=` syntax in Ruby.

---

### Exporting the envars

```
export host
export port
export relative_url_root
```

These commands make these envars available in the script which is run next:

### Executing the next script

```
exec ruby lib/run.rb "$@"
```

This line executes a Ruby script named `lib/run.rb` and passes it any arguments passed to the current bash script. Therefore the envars will be available by calling (for example) `ENV['host']`.  Same for envars #2 and #3.

---

In the [next post](/opc/what-happens-when-I-type-gdk-run-part-4/), we'll talk about what happens in the `lib/run.rb` script.
