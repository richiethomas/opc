---
layout: post
author: Richie Thomas
title: What happens when I type "gdk run"? Part 0
subtitle: What happens when an RBENV shim runs?
---

## Introducing the "gdk" shim

When you type 'gdk run', your OS checks the different loadpaths stored in its $PATH environment variable.  In my case, the load path for this executable is stored at `/Users/richiethomas/.rbenv/shims/gdk`.  `rbenv` is a Ruby version manager, a program that helps me manage the different versions of Ruby I have installed on my machine as well as the gems (Ruby libraries) I have installed for each version of Ruby that `rbenv` manages for me.

The `/shims/` folder indicates that `rbenv` uses something called "shims".  These are [files which sit in between the user and the program they're trying to use](https://stackoverflow.com/a/42935346/2143275), and perform some additional function before actually executing the user's desired program.

I've annotated the code of the shim below:

```
#!/usr/bin/env bash
set -e
[ -n "$RBENV_DEBUG" ] && set -x

program="${0##*/}"
if [ "$program" = "ruby" ]; then
  for arg; do
    case "$arg" in
    -e* | -- ) break ;;
    */* )
      if [ -f "$arg" ]; then
        export RBENV_DIR="${arg%/*}"
        break
      fi
      ;;
    esac
  done
fi

export RBENV_ROOT="/Users/richiethomas/.rbenv"
exec "/usr/local/Cellar/rbenv/1.1.2/libexec/rbenv" exec "$program" "$@"
```

Let's take this one line at a time:

---

## Shebang

```
#!/usr/bin/env bash
```

This is a shebang.  It tells the OS (specifically, the OS kernel's program loader) that the code which follows [should be interpreted as a bash script](https://stackoverflow.com/a/3009280/2143275).  There are also shebangs which tell the OS to interpret code in other ways, such as Ruby.  We'll see those later.

---

## The "set" command

```
set -e
```

I tried running `man set` in my CLI, but `set` is a builtin function, and is therefore not covered by the man pages.  However, `help` *does* cover the `set` command:

```
set: set [--abefhkmnptuvxBCHP] [-o option] [arg ...]
    ...
    -e  Exit immediately if a command exits with a non-zero status.
    ...
```

This line's job is to exit the program immediately if any part of it fails during execution.  If this line weren't included, [an error would not prevent the rest of the file from executing](https://stackoverflow.com/a/56124444/2143275)! 😱  I had no idea that was a thing- in Ruby for instance, errors can and do prevent the rest of your code from running.

---

## "Debugger" mode

`[ -n "$RBENV_DEBUG" ] && set -x`

This is a longer command, but it's actually pretty straightforward.  It's made up of two parts: `[ -n "$RBENV_DEBUG" ]`, and `set -x`.  Let's discuss what each of these means.

### Is the "RBENV_DEBUG" variable set?

`[ -n "$RBENV_DEBUG" ]`

The `[]` square brackets are actually a bash shorthand way of writing a conditional, and are [syntactic sugar for the `test` bash command](https://unix.stackexchange.com/questions/99185/what-do-square-brackets-mean-without-the-if-on-the-left).  They are used in conjunction with both the `-n` flag inside the brackets, as well as the `&&` notation to their right.  According to `man test`, `-n` means "if the length of the following thing is non-zero".  So this expression is saying "If the length of the `$RBENV_DEBUG` environment variable is greater than zero", or more simply, "if there's any kind of value stored in `$RBENV_DEBUG`", i.e. "if this variable exists".

### Printing out commands as they're run

`set -x`

Again relying on `help set`, we see that `-x` tells the interpreter to `Print commands and their arguments as they are executed.`

### Putting the pieces together

So looking back at our `[ -n "$RBENV_DEBUG" ] && set -x` command, we see that if there's a value for the `$RBENV_DEBUG` variable, we want to print the output of each command as it's executed.  This makes sense, considering the name of the variable indicates we want to do some debugging and printing the output of each command is exactly what we'd expect to happen when debugging.

---

## Deriving which program is being run

`program="${0##*/}"`

This could look intimidating to people unfamiliar with Bash (at least it does for me, coming from Ruby), but one clue here is that a variable named `program` is declared.  But let's come back to that later.

According to [StackOverflow](https://unix.stackexchange.com/questions/22387/how-do-0-and-0-work),  syntax like `${0##*/}` is known as "parameter expansion".  There's a couple different things happening in that string, which we'll address one-by-one.

### The "$" and "0" characters

In bash, `$0`, `$1`, `$2` etc are shorthand for parameters which are passed to a program.  `$0` refers to the filename of the running program (in this case, `gdk`).

If we place a debugger statement such as `echo $0` in the code in this file, we'd get back `/Users/richiethomas/.rbenv/shims/gdk` (I know this because I tried it!).

### The curly braces "{" and "}"

The curly braces are part of that ["parameter expansion"](https://unix.stackexchange.com/a/22390/142469) technique we mentioned a minute ago.  This is a way of substituting parameters for their values, while optionally performing edit operations on those values.

For example, one common operation is to get the name of the current running program based on the filepath name, and that's exactly what's happening here.  From the StackOverflow article below:

> Basically, in the example you have, ${0##*/} translates as:
> for the variable $0, and the pattern '/', the two hashes mean from the beginning of the parameter, delete the longest
> (or greedy) match—up to and including the pattern.

In other words, we're taking `/Users/richiethomas/.rbenv/shims/gdk` and shortening it to `gdk`.  And if we add `echo $program` into the program, `gdk` is exactly what we get.

---

## The if-block

```
if [ "$program" = "ruby" ]; then
  for arg; do
    case "$arg" in
    -e* | -- ) break ;;
    */* )
      if [ -f "$arg" ]; then
        export RBENV_DIR="${arg%/*}"
        break
      fi
      ;;
    esac
  done
fi
```

In terms of # of lines of code, this chunk represents the biggest section of the code in this file.  Let's get into it.

### Checking the value of our new "program" variable

`if [ "$program" = "ruby" ]; then`

Given what we know about square brackets being syntactic sugar for "if" statements, the above line of code represents an if statement checking whether the currently-running file is named "ruby".  But didn't we just determine that the currently-running file is named "gdk"?  So how could the `$program` variable contain the string "ruby", or indeed anything *other than* "gdk"?  I mean, we're *inside* the `gdk` shim, aren't we?

I don't want to get side-tracked from our main mission, so [click here](#detour-awesome-a-mystery-to-solve) if you want to go down this rabbit hole with me. 🙂  Otherwise, we'll keep plugging away at this if-block.

### Iterating over each argument

```
for arg; do
  case "$arg" in
```

Hopefully this is straightforward- we're iterating over each of the arguments provided to the shim, and starting a case statement to check each of them.

### Checking for the "-e" or "--" flags

```
-e* | -- ) break ;;
```

This is Bash syntax script for "if the value of the arg is equal to `-e` (with anything after it) or `--`, then break".  This `break` prevents any further arguments found afterward from being processed by the `for` block.

This rings true with what we find when we type `man ruby` and look for the `-e` flag in the "Options" section:

```
-e command     Specifies script from command-line while telling Ruby not to search the rest of the arguments for a script file name.
```

### Checking if the argument is a file

```
*/* )
  if [ -f "$arg" ]; then
    export RBENV_DIR="${arg%/*}"
    break
  fi
  ;;
esac
```

This means that, if an argument contains a "/" at all, then perform the enclosed if check.

The `-f` inside the if check checks to see if `$arg` represents a filename.  If it does, then we export a new environment variable (`RBENV_DIR`), and set it equal to everything *before* the `/` character (or the final `/` character, if there is more than one).

So for example, if one of the arguments that I pass to Ruby is "foo/bar/baz.rb", and if this file in this path actually exists, then `RBENV_DIR` will be set to `foo/bar`.

By the way- the `RBENV_DIR` envar "sets the directory from which .rbenv-version files are scanned", according to [the original Git commit](https://github.com/rbenv/rbenv/commit/07815769aeda3097f43d032fb31f6c89d1a7942f).

The reason for doing this is explained in the next section, but the TL;DR is that the file you pass to Ruby (i.s. `foo/bar/baz.rb`), or the directory it's contained in, may depend on a different version of Ruby than the one that your current directory depends on.

---

## Detour- Awesome, A Mystery To Solve!

We know that our conditional (`if [ "$program" = "ruby" ]; then`) will only return true when `ruby` is the command entered into the CLI.  This means it would only return true when running the `ruby` shim, which means it would only return true when typing `ruby <something>` from the command line.  But according to a quick browse of the various files in the `.shims` subfolder, every shim file looks exactly the same:

```
[ opc ] $ diff `which launchy` `which heapy`
[ opc ] $ diff `which coderay` `which erb`
[ opc ] $ diff `which gem` `which foreman`
[ opc ] $ diff `which geocode` `which rdoc`
[ opc ] $ diff `which scss` `which rspec`
[ opc ] $ diff `which thin` `which yard`
[ opc ] $ diff `which unicorn` `which spring`
[ opc ] $
```

This raises 3 questions in my mind:

1. Why is this logic only needed when the executing program is `ruby`?
2. What does this logic do?
3. Why do all RBENV shims include this conditional, if the only time it will ever be executed is when the `ruby` shim runs?  Why not just have this code in the Ruby shim, and have every other shim leave it out?

I hope you're as excited as I am to discover the answers!  If not, no worries- you can skip ahead to [the final section of this article](#setting-up-an-env-var-and-executing-the-original-command)!

### Why is this logic only needed when the executing program is `ruby`?

I asked the RBENV maintainers this very question.  See [here](https://github.com/rbenv/rbenv/issues/1173) for their full answer, but the short answer is that it's related to what happens when one ruby script shells out to another ruby script.  The phrase "shelling out" means your Ruby program will make a system call to the OS that's executing the Ruby script itself, either using the Ruby `system` method or the \`\` operator (aka backticks).  Some examples of normal system calls include `system("date")` or `curl www.google.com`.  And in our case, if one Ruby process is shelling out to another, the examples would look something like:

```
#!/usr/local/bin/ruby
system("ruby foo.rb")
```

Without the conditional logic in our shim, the version of Ruby that the 2nd script (i.e. the `foo.rb` file in our example above) would use the same Ruby version as the 1st Ruby script.  This is not always desirable, and in fact may introduce bugs if the two scripts depend on Ruby versions which differ in some significant way.

### What does this logic do?

The purpose of the conditional is to iterate over the parameters passed to the Ruby interpreter, searching for either `-e`/`--` or a filepath, and to handle each of these two scenarios in distinct ways.

### Why do all RBENV shims include this conditional?

The reason for including this conditional in all shims when it's only used for the Ruby shim comes down to a design choice that the code's author made.  As with most design choices, a trade-off was involved.

## Setting up an ENV var and executing the original command

```
export RBENV_ROOT="/Users/richiethomas/.rbenv"
exec "/usr/local/Cellar/rbenv/1.1.2/libexec/rbenv" exec "$program" "$@"
```

This sets the `RBENV_ROOT` envar to `/Users/richiethomas/.rbenv` (on my machine, at least).  This is the root directory where all shims are installed (among other things), so it's important for any subsequent program that gets run *and* which depends on `rbenv` to know where to find those shims.

Finally, with proper envars in place, we call `rbenv exec "$program" "$@"`.  This runs the `rbenv-exec` file and passes the program name and any additional arguments to that script.

## Wrapping up

This article wouldn't have been possible without the help of [Sudhagar's](https://medium.com/@Sudhagar) awesome article on [how RBENV works](https://medium.com/@Sudhagar/rbenv-how-it-works-e5a0e4fa6e76).  Thanks Sudhagar!

## Unanswered Questions:

1. How does `rbenv-exec` work?