---
layout: post
author: Richie Thomas
title: What happens when I type "gdk run"? Part 4
subtitle: Exploring the `run` command in detail
---

In the [last post](/opc/what-happens-when-I-type-gdk-run-part-3/), we talked about what happens in the `run` bash script, located in the main directory of `gitlab-development-kit`.  The last line of that bash script calls `exec ruby lib/run.rb "$@"`.  That line of code executes a Ruby script named `lib/run.rb`.  We'll take a look at that Ruby script in this post.

---

```
require_relative 'gdk/config'
```

This line requires `lib/gdk/config.rb`.  We'll talk about what this file does in [a future post](/opc/what-happens-when-I-type-gdk-run-part-3/).

---

```
def main(argv)
  applications = applications_from(argv)
  print_url if print_url?(applications)
  foreman_exec(applications)
end
```

This is the `main` method, which is called at the end of the file.  It calls `applications_from`, passing it any args which were passed to `gdk run` (for example, `db` or `gitaly` or `grafana`):

<details markdown="1">

  <summary>The applications_from method</summary>

```
def applications_from(argv)
  exec_thin! if argv[0] == 'thin'

  return %w[all] if argv.empty?

  argv.each_with_object([]) do |command, all|
    all << applications_for(command)
  end.flatten.uniq
end
```

This method first calls `exec_thin!` if the first argument passed to `gdk run` is "thin":

<details>

  <summary>The exec_thin! method</summary>

  ```
  def exec_thin!
    exec(
      { 'RAILS_ENV' => 'development' },
      *%W[bundle exec thin --socket=#{Dir.pwd}/gitlab.socket start],
      chdir: 'gitlab'
    )
  end
  ```

  This method simply runs `bundle exec thin start`, passing it:

  1. the name of the Rails environment as en environment variable, and
  2. the socket file that corresponds to the running Gitlab process.

</details>

---

Next, the method returns an array of one string (i.e. "all") if no arguments were passed to `gdk run`.

---

Finally, if there were args passed to `gdk run` but the first one wasn't `thin`, each arg is passed to `applications_for`:

<details>

  <summary>The applications_for() method</summary>

  ```
  def applications_for(command)
     case command
     when 'db'
       %w[redis postgresql openldap influxdb webpack registry minio elasticsearch jaeger]
     when 'geo_db'
       %w[postgresql-geo]
     when 'app'
       %w[gitlab-workhorse nginx grafana sshd gitaly storage-check gitlab-pages rails-web rails-background-jobs]
     when 'grafana'
       %w[grafana]
     when 'gitaly'
       %w[gitaly]
     when 'praefect'
       praefect_services
     when 'jobs'
       %w[rails-background-jobs]
     else
       puts
       puts "GitLab Development Kit does not recognize command '#{command}'."
       puts "Make sure you are using the latest version or check available commands with: \`gdk help\` "
       puts
       exit 1
     end
   end
  ```

This method checks each command passed as an argument via the `each_with_object` iterator above.  For each command, it returns a different array of strings depending on what the value of `command` is.  For instance, if the command is `geo_db` (i.e. `gdk run geo_db`), "applications_for()" will return the array `%w[postgresql-geo]`.  In the default case (if the command passed to `gdk run` isn't recognized, i.e. if you enter `gdk run foobar` or some other non-sensical command), an error message is printed to the screen and the process exits with an error code of 1:

```
$ gdk run foobar
(in /Users/richiethomas/Workspace/ThreeEasyPieces/14/gitlab-development-kit)

GitLab Development Kit does not recognize command 'foobar'.
Make sure you are using the latest version or check available commands with: `gdk help`
```

</details>

</details>

---

<details>

  <summary>Printing the URL</summary>

```
print_url if print_url?(applications)
```

The array of application names which was returned from "applications_from()" is checked to see if it contains either "gitlab-workhorse" or "all".  If either term is present, the following message is printed to the screen:

```
"Starting GitLab in #{Dir.pwd} on http://#{ENV['host']}:#{ENV['port']}#{ENV['relative_url_root']}"
```

The `host`, `port`, and `relative_url_root` envars were set in the bash script which we described in the [previous post](/opc/what-happens-when-I-type-gdk-run-part-3/).

</details>

---

<details>

  <summary>Starting the app with Foreman</summary>

  ```
  foreman_exec(applications)
  ```

The "foreman_exec" method runs the `lib/daemonizer.rb` Ruby script, passing it the arguments `foreman` and `start`:

```
def foreman_exec(svcs = [], exclude: [])
  args = %w[ruby lib/daemonizer.rb foreman start]

  unless svcs.empty? && exclude.empty?
    args << '-m'
    svc_string = ['all=0', svcs.map { |svc| svc + '=1' }, exclude.map { |svc| svc + '=0' }].join(',')
    args << svc_string
  end

  exec({
    'GITLAB_TRACING' => 'opentracing://jaeger?http_endpoint=http%3A%2F%2Flocalhost%3A14268%2Fapi%2Ftraces&sampler=const&sampler_param=1',
    'GITLAB_TRACING_URL' => 'http://localhost:16686/search?service={{ service }}&tags=%7B"correlation_id"%3A"{{ correlation_id }}"%7D'
  }, *args)
end
```

The `unless` block creates a list of additional arguments to append to the `args` local variable.  For example, if the command is a simple `gdk run`, `svcs` should be `all` as a result of the call to `applications_from`.  In this case, the value of `args` will be `["ruby", "lib/daemonizer.rb", "foreman", "start", "-m", "all=0,all=1,"]` at the end of the `unless` block.

This block *should* always be called, since there don't seem to be any scenarios in which `applications_for()` will return an empty array which gets passed as the `svcs` argument to `foreman_exec`.

Finally, the `exec` system call is made, setting [two envars](/opc/2019-08-29-what-do-GITLAB_TRACING-and-GITLAB_TRACING_URL-do) (`GITLAB_TRACING` and `GITLAB_TRACING_URL`) and passing the value of `args` as a splatted array of strings.

Remember that if Foreman were replaced by Runit (i.e. if the `GDK_RUNIT` envar is set to "1"), you'll never reach this point in the code as you would have had to run `gdk start` instead.
</details>

---

In [the next post](/opc/2019-08-28-what-happens-when-I-type-gdk-run-part-5), we'll look at what happens in the `lib/daemonizer.rb` file.

---

One last note- one of the `gdk run` commands this file checks for is `gdk run praefect`.  This case branch calls the `praefect_services` method, which looks like the following:

```
def praefect_services
  @config ||= GDK::Config.new
  services = %w[praefect]
  @config.praefect.nodes.each_with_index {|praefect_node, index| services.push("praefect-gitaly-#{index}") }
  services
end
```

It finds or creates a new instance of GDK::Config (described [here](/opc/2019-08-28-what-is-the-gdk-config-module)).  It then creates a string array with one member: "praefect".  Then, it access the attribute `@config.praefect.nodes`, which returns an integer corresponding to the value of the `PRAEFECT_GITALY_NODES` environment variable, or 3 if that envar isn't set.  For each integer from 0 to the value of this envar (not inclusive), it appends the string "praefect-gitaly-#{index}" (where "index" is the integer) to the `services` array.  Finally, this method returns that array.

I wasn't sure what the intention of this `praefect` command is, so I looked up the commit history (honestly, I'm not yet sure what most of them do, but this was the only command I had never heard of before in the context of Gitlab).  Here's what I see from a `git blame`:

```
417da37b (John Cai         2019-08-05 07:49:10 -0700  46)   when 'praefect'
417da37b (John Cai         2019-08-05 07:49:10 -0700  47)     praefect_services
```

I look up commit `417da37b` on Gitlab.com, and see [this page](https://gitlab.com/gitlab-org/gitlab-development-kit/commit/417da37bf1331560863bf118064ea75194ebeddd), which doesn't yield anything familiar to my eyes.

Then I do a grep in the Git history for the string `praefect`, and I see the following:

```
[ gitlab-development-kit ] $ git log -Spraefect
commit 417da37bf1331560863bf118064ea75194ebeddd
Author: John Cai <jcai@gitlab.com>
Date:   Mon Aug 5 07:49:10 2019 -0700

    Adding Praefect configuration

    This adds a commented-out by default praefect configuration with 3
    internal gitaly nodes.

commit 2cfbb9b7475684498581de94e261178938f4c427
Author: Zeger-Jan van de Weg <git@zjvandeweg.nl>
Date:   Mon Jul 8 21:52:13 2019 +0200

    Move Gitaly config to Rake tasks

    While not necessarily required, moving all Gitaly tasks to Rake allowed
    for easier access to them and allowed easier configuration of Gitaly and
    GitLab alike.

    This flexibility is leveraged for the new rake tasks for `praefect`.
    `rake praefect:enable` and `rake praefect:disable` are introduced to use
    Praefect as a proxy in the GDK and allow for later iterations which will
    run a cluster of Gitaly servers.

    Supersedes: https://gitlab.com/gitlab-org/gitlab-development-kit/merge_requests/645
```

Here I see the commit I just looked up, plus one from about a month prior.  The older commit message mentions two new rake tasks which use Praefect as a proxy in GDK.  This might be more helpful if I had more experience with the codebase, but as-is it doesn't help me understand things any better.  I also see a URL to a previous merge request (I keep wanting to call them pull requests), which I open up and whose title is `Allow praefect to be run as part of the GDK`.

By this point, I've also Googled `Gitlab Praefect`, `Praefect software`, and several other possibilities, but I haven't found anything.  This leads me to conclude that a) Praefect is not a 3rd-party library or other external dependency, which implies that it's a concept which is internal to Gitlab.  I've also seen "Praefect" mentioned alongside "Gitaly" several times, as well as the acronym `HA` ("high availability") used in conjunction with Praefect on several pages.  This leads me to suspect that Praefect is an internal package which allows one to run several redundant instances of Gitaly in addition to the master instance.

I do a simple case-insensitive `grep` for `praefect` and find several files which reference it (Procfile.erb, support/templates/gitlab.html.erb, .gitignore, Rakefile, etc.), but nothing that explicitly describes what it is.  At this point, I'm starting to think that Praefect is like Fight Club- the first rule is you don't talk about it.

I'd love to find the very first reference to Praefect in the codebase.  Surely that would have to give some high-level context on what it is, right?  But that's what `git log -Spraefect` and `git log -SPraefect` were supposed to do, and those are the two commit results I mentioned above.

Finally, I try another Google search of `praefect site:gitlab.com`, *scoped to results from Dec 2018 to March 2019*.  Success!  I find [the following link](https://gitlab.com/gitlab-org/gitaly/issues/1483) to a Gitlab issue, which mentions [the following](https://gitlab.com/gitlab-org/gitaly/issues/1481) earlier Gitlab issue:


> ## Gitaly Praefect - Pass through proxy package
> Introducing a new component isn't without risks and true to GitLab values we should iterate towards the new component.
>
> To do so the first iteration will only be a pass through proxy, where the coordinator, for the time being called praefect, doesn't add or remove anything from any request.
>
> To fix this issue, a well tested proxy package suffices to improve parallelizing the work in our team. An actual execution path, thus a binary to execute, is added by another issue.
>
> /cc @pokstad1
>
> P.S. Praefect is choosen because of the following quote:
>
> a person appointed to any of various positions of command, authority, or superintendence, as a chief magistrate in ancient Rome or the chief administrative official of a department of France or Italy.

Sounds like it's something called a "pass-through proxy".  I don't know enough about Gitaly to know why it would need one of those, or even what value a proxy would add if it simply passes requests through from client to server and back.

I think the answer to the 2nd question can be found in the first two sentences, which I'll repeat below:

> Introducing a new component isn't without risks and true to GitLab values we should iterate towards the new component.
>
> To do so the first iteration will only be a pass through proxy, where the coordinator, for the time being called praefect, doesn't add or remove anything from any request.

One of Gitlab's core values seems to be iterating toward a final solution.  If the goal of the team was to introduce a proxy which *does* alter requests and responses, then introducing a *pass-through* proxy (which makes no changes to the requests that it forwards) is a useful iterative step towards this end goal.

OK, so that makes sense.  But what *kinds* of alterations is Praefect intended to make?  Will it in fact make *any* alterations at all, or is its goal more to load-balance?

Answering this question really requires knowledge of Gitaly itself, which I don't currently have but which I may try to answer in an upcoming post.

UPDATE: [jackpot](https://gitlab.com/groups/gitlab-org/-/epics/289):

> HA Gitaly: Optimistic naive replication and verification
> Design document | This is the first checkpoint in shipping HA Gitaly &842
>
> Problem to solve
> Gitaly is currently a single point of failure. In order to make Gitaly highly available we will replicate Git repositories to multiple Gitaly nodes.
>
> Proposal
> In this first iteration, every Git write operation will trigger a complete sync from the primary to the replicas, followed by a verification to make sure each replica is an accurate copy of the primary.
>
> Based on the planned architecture, we will implement:
>
> reverse-proxy Praefect that will manage replication
> config based management
> eventual consistency, no performance optimizations
> monitoring to assess the replication delay of a naive implementation (we hope the delay will be around the 1 minute mark at 99p)
> This will allow small scale testing on GitLab.com by whitelisting a small number of projects to replicate.

To summarize: Gitaly appears to be the location where Gitlab stores its Git repositories.  And Praefect is the high-availability proxy which manages the synchronization between the master and replica sources of truth.

Success!

---

One final note- this is the file in which the cool Gitlab fox ASCII art is printed!

<details markdown="1">

<summary>See the art</summary>

```
def print_logo
  printf "

           \033[38;5;88m\`                        \`
          :s:                      :s:
         \`oso\`                    \`oso.
         +sss+                    +sss+
        :sssss:                  -sssss:
       \`ossssso\`                \`ossssso\`
       +sssssss+                +sssssss+
      -ooooooooo-++++++++++++++-ooooooooo-
     \033[38;5;208m\`:/\033[38;5;202m+++++++++\033[38;5;88mosssssssssssso\033[38;5;202m+++++++++\033[38;5;208m/:\`
     -///\033[38;5;202m+++++++++\033[38;5;88mssssssssssss\033[38;5;202m+++++++++\033[38;5;208m///-
    .//////\033[38;5;202m+++++++\033[38;5;88mosssssssssso\033[38;5;202m+++++++\033[38;5;208m//////.
    :///////\033[38;5;202m+++++++\033[38;5;88mosssssssso\033[38;5;202m+++++++\033[38;5;208m///////:
     .:///////\033[38;5;202m++++++\033[38;5;88mssssssss\033[38;5;202m++++++\033[38;5;208m///////:.\`
       \`-://///\033[38;5;202m+++++\033[38;5;88mosssssso\033[38;5;202m+++++\033[38;5;208m/////:-\`
          \`-:////\033[38;5;202m++++\033[38;5;88mosssso\033[38;5;202m++++\033[38;5;208m////:-\`
             .-:///\033[38;5;202m++\033[38;5;88mosssso\033[38;5;202m++\033[38;5;208m///:-.
               \`.://\033[38;5;202m++\033[38;5;88mosso\033[38;5;202m++\033[38;5;208m//:.\`
                  \`-:/\033[38;5;202m+\033[38;5;88moo\033[38;5;202m+\033[38;5;208m/:-\`
                     \`-++-\`\033[0m

  "
  puts
end

```

---

It doesn't look like much above, but here's what it looks like in the console:

![Imgur](https://i.imgur.com/LsQI4Yg.png)

</details>



---