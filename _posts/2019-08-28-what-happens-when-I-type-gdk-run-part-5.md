---
layout: post
author: Richie Thomas
title: What happens when I type "gdk run"? Part 5
---

In [the last post](/opc/2019-08-28-what-happens-when-I-type-gdk-run-part-4), we looked at what happens inside the `lib/run.rb` file.  One of the last things that happens there is a call to Ruby's `exec` method.  Among other things, the script passes the arguments `["ruby", "lib/daemonizer.rb", "foreman", "start", "-m", "all=0,all=1,"]`.  In this post, we'll look at what happens inside that `lib/daemonizer.rb` file.

---

<details>

<summary>Helpful comments, PR notes, etc.</summary>

First, [what is a daemon?](https://kb.iu.edu/d/aiau)

> A daemon is a long-running background process that answers requests for services.

Great, thanks.

Next- [what does it mean to daemonize a process?](https://codingfreak.blogspot.com/2012/03/daemon-izing-process-in-linux.html)

From reading the code inside `daemonizer.rb` and the page I just linked to, I gather that it means we will background a process which was previously running in the foreground.

```
# daemonize does a standard double fork, with an added twist: a
# separate process that monitors both the original process and the
# daemon process, and terminates the other if one disappears.
```

These comments were taken from near the top of the file, just above the `daemonize` method (where the bulk of the file's business logic is contained).  This didn't make much sense to me initially, but once I read the [merge request description](https://gitlab.com/gitlab-org/gitlab-development-kit/merge_requests/627/diffs?commit_id=30c28f62e27c707f4df726db940c468b9b29a398), the [code diff](https://gitlab.com/gitlab-org/gitlab-development-kit/commit/2a36d68657c01904d12ae9526bc4e90485528842) of the MR, and [a related issue](https://gitlab.com/gitlab-org/gitlab-development-kit/issues/425#note_144066011) which may have prompted the MR, things became clearer.

In [a previous version](https://gitlab.com/gitlab-org/gitlab-development-kit/commit/2a36d68657c01904d12ae9526bc4e90485528842) of `lib/run.rb`, there was no concept of "daemonizing" the `foreman` process manager.  GDK simply let Foreman do its own thing.  That caused problems for some developers, for instance if they closed a terminal window which was actively running a GDK process.  This would terminate that process, but [not in a clean way](https://gitlab.com/gitlab-org/gitlab-development-kit/issues/425#note_144066011), instead resulting in hanging child processes which had been started (but not shut down) by the aforementioned GDK process.

In an attempt to remedy this problem, "daemonizer.rb" was introduced.  The comment above explains how the new flow of execution differs from the old.  In particular, it mentions the concept of a "double fork", which I am just now learning about as I write this article.  A double fork is a technique for preventing [zombie processes](https://www.geeksforgeeks.org/zombie-processes-prevention/) (i.e. processes that have died, but whose PID still remains listed in the OS's process table).

Zombie processes can happen if, for some reason, a parent process fails to remove its child process PID from the process table after the child process dies (for instance, if the parent dies before it has a chance to reap the child PID from the table).  Zombie processes are bad because they take up space in the process table that could otherwise be used by other, healthy processes.  If too many zombie processes exist in the table, no new programs can be run.  Hence, preventing zombie processes is considered good hygiene.

</details>

---

<details>

<summary>The "main" method</summary>

```
def main
  daemon_pid = daemonize(ARGV)
  trap('INT') { trigger_normal_shutdown(daemon_pid) }

  loop do
    break unless pid_running?(daemon_pid)
    sleep 1
  end
end

```

This is the `main` method, which is called in the last line of the file.  It calls the `daemonize` method, which is explained below.  It then calls the `trap` system call method, passing the string 'INT' and a block.  I *believe* this tells the OS to handle ~~any return code integers (i.e. `exit 0` or `exit 1`)~~ a signal interrupt by calling the method invoked in the block (i.e. `trigger_normal_shutdown`).  Lastly, it executes a never-ending loop which checks if there's a currently-running process, and breaks out of the loop (and hence ends execution of the `main` method) if there is none.

---

</details>


<details>

<summary>The "daemonize" method</summary>

```
# daemonize does a standard double fork, with an added twist: a
# separate process that monitors both the original process and the
# daemon process, and terminates the other if one disappears.
def daemonize(args)
  top_pid = Process.pid

  pipe_read, pipe_write = IO.pipe

  middle_pid = Process.fork
  if middle_pid
    # This if-branch is in the top process.
    pipe_write.close

    daemon_pid = pipe_read.read.to_i
    pipe_read.close

    Process.wait(middle_pid)

    return daemon_pid
  end

  pipe_read.close

  STDIN.reopen '/dev/null'

  # Become session leader (standard part of daemonizing).
  Process.setsid

  daemon_pid = Process.fork
  unless daemon_pid
    # This is the daemon process.

    pipe_write.close
    exec(*args)

    # This line is never reached.
  end

  # Still in the middle process.
  pipe_write.write(daemon_pid.to_s)
  pipe_write.close

  Process.fork do
    # This is our watchdog
    watch_pids(top_pid, daemon_pid)
    exit
  end

  # The middle process is only used during startup.
  exit
end
```

Obviously a big method, so let's break it down:

```
top_pid = Process.pid

pipe_read, pipe_write = IO.pipe

middle_pid = Process.fork
```

This stores the currently-running process's PID as a variable.  It then creates [a pair of pipe endpoints](https://ruby-doc.org/core-2.5.1/IO.html#method-c-pipe) which are connected to each other, and returns them as a 2-tuple.  Finally, it forks the currently-running process to create a child process (essentially asking the current program to run a new program) and stores the PID of that child process in a new variable, `middle_pid`.

```
if middle_pid
  # This if-branch is in the top process.
  pipe_write.close

  daemon_pid = pipe_read.read.to_i
  pipe_read.close

  Process.wait(middle_pid)

  return daemon_pid
end
```

If the `middle_pid` variable exists, that means we're currently in the top process.

We then close the write-side of the IO pipe we created earlier, since presumably the parent process will only be using the read-side of the pipe (and [it's considered good hygiene](https://stackoverflow.com/questions/19265191/why-should-you-close-a-pipe-in-linux) to close any parts of a pipe that you won't be using).

Next we wait for the middle process to finish executing, and finally we return the `daemon_id` (i.e. the PID of the grandchild that is written to the pipe further down in the code).  By the way, throughout this post I'll use the terms "daemon process" and "grandchild process" interchangeably.

---

```
pipe_read.close

STDIN.reopen '/dev/null'

# Become session leader (standard part of daemonizing).
Process.setsid

daemon_pid = Process.fork
unless daemon_pid
  # This is the daemon process.

  pipe_write.close
  exec(*args)

  # This line is never reached.
end
```

If we reach this code, that means we were never inside the "if"-block (i.e. `if middle_pid`).  That means we're now currently in the middle process.

We close the read-end of the pipe and redirect all output to "/dev/null".

We next call `Process.setsid`.  The reason for this is a bit complicated, but the TL;DR is that part of preventing a zombie process via the double fork strategy is [preventing the daemon process from taking on a controlling terminal](https://thelinuxjedi.blogspot.com/2014/02/why-use-double-fork-to-daemonize.html).  If the reasons for this are unclear, hopefully the link will clarify things better than I could hope to.  It talks about why `setsid` is useful, and why it needs to be called at this particular point in the sequence of steps.

Next, we fork again to create the daemon process.  If there is nothing stored in the `daemon_id` variable, that means we're inside the daemon process and we enter the `unless`-block.

Next, we close the write-end of the pipe, and execute the arguments which were passed to this program (the arguments which came after `ruby lib/daemonizer.rb` in `lib/run.rb`, i.e. `foreman start`).  Since `exec()` exits after it completes, once we enter the "unless"-block, we can be sure that none of the lines of code between `exec()` and the end of the file will be run.

---

```
  # Still in the middle process.
  pipe_write.write(daemon_pid.to_s)
  pipe_write.close

  Process.fork do
    # This is our watchdog
    watch_pids(top_pid, daemon_pid)
    exit
  end

  # The middle process is only used during startup.
  exit
end
```

This is the last section of the `daemonize()` method.  Assuming we've reached this point, we're still in the middle process (as the comment states).  Since the middle process created the daemon process, the middle process has access to the value returned from `Process.fork`, which it stored in the `daemon_pid` local variable.  We write this pid value to the write end of our pipe, and this will be read by the top (aka grandparent) process.

Since we're done writing to the pipe after we do this, we then close the write end of the pipe.

This is where the program gets interesting!  In other words, this is where it starts to deviate from the standard double-fork method of daemonization.  The program forks a *third* time (something normal double-forks don't do, making *this* program a "triple-fork").  The third fork creates a sibling process to our daemon process, in the sense that the parents of both the daemon and its sibling are the middle process.

The job of this sibling process will be to continually check the statuses of *both* the grandparent process and the daemon process in a loop.  If either process has died, it's the job of the sibling process to kill the *other* process.  For example, if the parent dies, the sibling kills the daemon (and vice-versa).

</details>

---

## Conclusion

And that's it!  At this point, I *believe* I've covered all the major components of what happens when you type `gdk run` into your terminal!  I hope that was as informative for you to read as it was for me to write. :-)

<details>

<summary>Unanswered questions:</summary>

1) Is starting up a web server the goal of the `daemonize.rb` file?  If not, what *is* the goal?  And if it is, why did they implement this goal by forking processes, instead of using a pre-built web server library like Puma or Unicorn?

2) The `main` method handles signal interrupts (i.e. if the user hits Control-C on their keyboard) and it handles cases where the process dies (either on its own or, presumably, by some sort of error).  Are there any other cases other than these which are not handled, but should be?

3) What exactly *is* the job of the grandparent process?  I see its implementation involves waiting for the middle process to do its thing before returning the daemon_pid, but why does the grandparent process need to hang around until the middle process exits?

</details>