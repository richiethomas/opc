---
layout: post
author: Richie Thomas
title: What do the GITLAB_TRACING and GITLAB_TRACING_URL envars do?
---

In a [previous post](/opc/2019-08-26-what-happens-when-I-type-gdk-run-part-4/), I described how the `lib/run.rb` file has a `foreman_exec` method, which sets two environment variables named `GITLAB_TRACING` and `GITLAB_TRACING_URL` before starting up the Foreman process manager.  In this post, I'll try to figure out what those envars do.

---

<details markdown="1">

  <summary>foreman_exec()<summary>

  ```
  def foreman_exec(svcs = [], exclude: [])
    args = %w[ruby lib/daemonizer.rb foreman start]

    unless svcs.empty? && exclude.empty?
      args << '-m'
      svc_string = ['all=0', svcs.map { |svc| svc + '=1' }, exclude.map { |svc| svc + '=0' }].join(',')
      args << svc_string
    end

    exec({
      'GITLAB_TRACING' => 'opentracing://jaeger?http_endpoint=http%3A%2F%2Flocalhost%3A14268%2Fapi%2Ftraces&sampler=const&sampler_param=1',
      'GITLAB_TRACING_URL' => 'http://localhost:16686/search?service={{ service }}&tags=%7B"correlation_id"%3A"{{ correlation_id }}"%7D'
    }, *args)
  end
  ```

  If you recall, this is what the method in question looks like.  In particular, we'll look at the following:

  ```
  exec({
    'GITLAB_TRACING' => 'opentracing://jaeger?http_endpoint=http%3A%2F%2Flocalhost%3A14268%2Fapi%2Ftraces&sampler=const&sampler_param=1',
    'GITLAB_TRACING_URL' => 'http://localhost:16686/search?service={{ service }}&tags=%7B"correlation_id"%3A"{{ correlation_id }}"%7D'
  }, *args)
  ```

  This method declares the two envars, before passing them to the `daemonizer.rb` file.  I thought the values of these envars looked strange; for instance, `GITLAB_TRACING` looks like a URL, but I have never seen the `opentracing://` protocol before.  After some Googling, I found [the following](https://opentracing.io):

![Imgur](https://i.imgur.com/nTMJ8kO.png)

Turns out- OpenTracing is a [distrubuted tracing](https://opentracing.io/docs/overview/what-is-tracing/) tool.  According to the site:

> Distributed tracing, also called distributed request tracing, is a method used to profile and monitor applications,
> especially those built using a microservices architecture. Distributed tracing helps pinpoint where failures occur and
> what causes poor performance.

Even better, I found a [tracing-specific README](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/distributed_tracing.md) in the Gitlab codebase!

The README contains information on how to enable or disable tracing in the Gitlab codebase, which of Gitlab's services take advantage of tracing, details on performance implications, and
</details>

<details>

<summary>Unanswered questions</summary>

1. Why are these envars declared here, and not in some other, centralized, envar-specific file?  It seems like declaring some envars here, and others elsewhere, implies a lack of a central envar declaration file (i.e. `.env`), which means it can be more painful to keep track of all envars in one place, peruse them all easily, see if any are duplicated and/or over-written, etc.

2. I've never heard of the `opentracing://` protocol.  How does it work, and can anyone declare their own protocol?  If so, how does one do that, and get the client to recognize it?

3. How did Gitlab land on OpenTracing, as opposed to another error reporting tool like Rollbar?  Are these tools even used for the same purpose, or am I talking about apples and oranges here?

4. From the Gitlab tracing README- "The first tracing implementation that GitLab supports is Jaeger..."  Are there other tracing implementations out there besides Jaeger?  If so, why did Gitlab choose this one?  What's the difference between Jaeger and OpenTracing?

</details>