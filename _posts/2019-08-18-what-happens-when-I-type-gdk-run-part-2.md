---
layout: post
author: Richie Thomas
title: What happens when I type "gdk run"? Part 2
subtitle: Defining the bulk of `gdk` commands
---

In [Part 1 of this guide](/opc/what-happens-when-I-type-gdk-run-part-1/), we talked about the first steps that happen when you type `gdk run` in your console.  This covered all the logic that's executed inside the `gdk` Ruby script, which is located wherever your `gdk` gem is installed.  At that end of that gem's logic, the `gdk` gem looks for a file called `lib/gdk.rb` which is included inside every installation of `gitlab-development-kit`.  If it finds it, it imports it into the running process and calls `GDK.main`.  That's where we'll pick up in this post.

---

<details>
<summary>Click here for the full `lib/gdk.rb` file</summary>

```
# GitLab Development Kit CLI parser / executor
#
# This file is loaded by the 'gdk' command in the gem. This file is NOT
# part of the gitlab-development-kit gem so that we can iterate faster.

require_relative 'gdk/env'
require_relative 'gdk/config'
require_relative 'gdk/erb_renderer'
require_relative 'runit'

module GDK
  PROGNAME = 'gdk'.freeze
  MAKE = RUBY_PLATFORM =~ /bsd/ ? 'gmake' : 'make'
  SUPERVISOR = ENV['GDK_RUNIT'] == '1' ? :runit : :foreman

  # This function is called from bin/gdk. It must return true/false or
  # an exit code.
  def self.main
    if !install_root_ok? && ARGV.first != 'reconfigure'
      puts <<-EOS.gsub(/^\s+\|/, '')
        |According to #{ROOT_CHECK_FILE} this gitlab-development-kit
        |installation was moved. Run 'gdk reconfigure' to update hard-coded
        |paths.
      EOS
      return false
    end

    pg_port_file = File.join($gdk_root, 'postgresql_port')
    pg_port = File.exist?(pg_port_file) ? File.read(pg_port_file) : 5432

    case subcommand = ARGV.shift
    when 'run'
      exec('./run', *ARGV, chdir: $gdk_root)
    when 'install'
      exec(MAKE, *ARGV, chdir: $gdk_root)
    when 'update'
      # Otherwise we would miss it and end up in a weird state.
      puts "\n> Running `make self-update` separately in case the Makefile is updated..\n\n"
      system(MAKE, 'self-update', chdir: $gdk_root)

      puts "\n> Running `make self-update update`..\n\n"
      exec(MAKE, 'self-update', 'update', chdir: $gdk_root)
    when 'diff-config'
      require_relative './config_diff.rb'

      files = %w[
        gitlab/config/gitlab.yml
        gitlab/config/database.yml
        gitlab/config/unicorn.rb
        gitlab/config/puma.rb
        gitlab/config/resque.yml
        gitlab-shell/config.yml
        gitlab-shell/.gitlab_shell_secret
        redis/redis.conf
        .ruby-version
        Procfile
        gitlab-workhorse/config.toml
        gitaly/config.toml
        nginx/conf/nginx.conf
      ]

      file_diffs = files.map do |file|
        ConfigDiff.new(file)
      end

      file_diffs.each do |diff|
        $stderr.puts diff.make_output
      end

      file_diffs.each do |diff|
        puts diff.output unless diff.output == ""
      end

      true
    when 'reconfigure'
      remember!($gdk_root)
      exec(MAKE, 'touch-examples', 'unlock-dependency-installers', 'postgresql-sensible-defaults', 'all', chdir: $gdk_root)
    when 'psql'
      exec('psql', '-h', File.join($gdk_root, 'postgresql'), '-p', pg_port.to_s, *ARGV, chdir: $gdk_root)
    when 'redis-cli'
      exec('redis-cli', '-s', File.join($gdk_root, 'redis/redis.socket'), *ARGV, chdir: $gdk_root)
    when 'env'
      GDK::Env.exec(ARGV)
    when 'start', 'stop', 'restart', 'status'
      assert_supervisor_runit!
      Runit.sv(subcommand, ARGV)
    when 'tail'
      assert_supervisor_runit!
      Runit.tail(ARGV)
    when 'help'
      puts File.read(File.join($gdk_root, 'HELP'))
      true
    else
      puts "Usage: #{PROGNAME} run|init|install|update|reconfigure|psql|redis-cli|diff-config|version|help [ARGS...]"
      false
    end
  end

  def self.install_root_ok?
    expected_root = File.read(File.join($gdk_root, ROOT_CHECK_FILE)).chomp
    File.realpath(expected_root) == File.realpath($gdk_root)
  rescue => ex
    warn ex
    false
  end

  def self.assert_supervisor_runit!
    return if SUPERVISOR == :runit

    abort "this subcommand is only available when using runit"
  end
end
```

</details>

---

## Initial file comments

```
# GitLab Development Kit CLI parser / executor
#
# This file is loaded by the 'gdk' command in the gem. This file is NOT
# part of the gitlab-development-kit gem so that we can iterate faster.
```

This is the file where you'd feel free to add new `gdk` commands.  In contrast, there's a comment in the gem's `gdk` file (the file we looked at in the previous post) which states the following:

```
# Note to contributors: this script must not change (much) because it is
# installed outside the gitlab-development-kit repository with 'gem
# install'. Edit lib/gdk.rb to define new commands.
```

It apparently takes longer for the Gitlab team to push updates to the `gitlab-development-kit` gem than it does to push updates to the rest of their code, which is hosted on GitHub.  It's possible that the gem code has to go through an external code review process for security purposes, like an app on Apple's App Store goes through.  I'm not entirely sure, but the purpose of this is to allow the team to push new `gdk` commands faster than they otherwise could.

---

## Re-opening the GDK module

```
module GDK
  PROGNAME = 'gdk'.freeze
  MAKE = RUBY_PLATFORM =~ /bsd/ ? 'gmake' : 'make'
  SUPERVISOR = ENV['GDK_RUNIT'] == '1' ? :runit : :foreman
  ...
end
```

If you recall from the previous post, we declared the `GDK` module in `bin/gdk`.  In this file we re-open that module, and start modifying it by declaring a few constants.  These constants are:

1) `PROGNAME`- the frozen string `gdk`
2) `MAKE`- either `gmake` or `make`, depending on whether the value of the `RUBY_PLATFORM` constant contains the `bsd` substring (i.e. whether the current process is running on the BSD operating system).
3) `SUPERVISOR`- equal to `:runit` if the Runit experiment is turned on, otherwise it's equal to `:foreman`.  See `doc/runit.md` for more information on this experiment.  Essentially this value determines how GDK processes are run (either using the Foreman orchestrator, or Runit).

---

## Declaring "self.main"

```
  def self.main
    if !install_root_ok? && ARGV.first != 'reconfigure'
      puts <<-EOS.gsub(/^\s+\|/, '')
        |According to #{ROOT_CHECK_FILE} this gitlab-development-kit
        |installation was moved. Run 'gdk reconfigure' to update hard-coded
        |paths.
      EOS
      return false
    end
```

This is the start of the declaration of the `self.main` method, i.e. the method which is called from the `gdk` file in the gem.  The first block of logic is a conditional statement, which prints an error to the console if the expected root of the project is different from the actual root of the project, and the command that the user run is *not* `gdk reconfigure`.

### Detour- the "self.install_root_ok?" method

```
def self.install_root_ok?
  expected_root = File.read(File.join($gdk_root, ROOT_CHECK_FILE)).chomp
  File.realpath(expected_root) == File.realpath($gdk_root)
rescue => ex
  warn ex
  false
end
```

This is the logic which checks the first part of the conditional block above.  The value of `expected_root` is taken from the contents of the `.gdk-install-root` file.  These contents were populated in the `gdk` gem file from which this method is called.  Essentially, if the directory mentioned inside that file is different from the directory in which the file itself is found, this method returns false.  And if something goes wrong and an exception is raised while this check is happening, a warning is printed to the console and this method still returns false.  Otherwise, the happy path is that the file is found in the expected directory, and this method returns true.

To be honest, I'm still not sure why the values of `expected_root` and `actual_root` would ever be different, or how that could come about.  This may become more apparent as I continue reading the code.  But the error which is printed by the `if` conditional is helpful- sounds like it's somehow possible that the project directory could be moved at some point after the `.gdk-install-root` file is populated with the expected directory.  If and when that happens, the user must run `gdk reconfigure` to re-populate that file with the new directory path.  But the question remains- why would the project directory ever have cause to be moved?

---

## Setting up the Postgres port

```
pg_port_file = File.join($gdk_root, 'postgresql_port')
pg_port = File.exist?(pg_port_file) ? File.read(pg_port_file) : 5432
```

This code sets a local variable named `pg_port` equal to the contents of a `postgresql_port` (if one exists), or `5432` (if no such file exists).  Essentially, the user can override the default Postgres port by creating a file with this name and setting it equal to a non-standard PSQL port.

---

## Pattern-matching incoming commands

```
case subcommand = ARGV.shift
```

The above case statement in the project's `gdk.rb` file is what allows Gitlab developers to add new `gdk` commands.  The new local variable named `subcommand` corresponds to the first argument that the user has passed to the `gdk` command in their terminal.  It's compared against different allow-listed values, and executes different logic accordingly.

This is the meat of the logic of the `GDK.main` method.

---

### The "run" Command

```
when 'run'
  exec('./run', *ARGV, chdir: $gdk_root)
```

If the command the user entered is `run`, then the `./run` bash script is executed.  This bash script is passed any arguments that the user passed to `gdk run`.

The `chdir: $gdk_root` bit changes directories to the value of `$gdk_root` before executing the `./run` bash script.  Which is weird to me, because I'm pretty sure the directory should *already* be `$gdk_root` by the time we get to this `exec` statement, and that this should always be the case.  I believe this to be true because I did the following:

```
   31:     byebug
   32:     when 'run'
=> 33:       exec('./run', *ARGV, chdir: $gdk_root)
   34:     when 'install'
   35:       exec(MAKE, *ARGV, chdir: $gdk_root)
   36:     when 'update'
   37:       # Otherwise we would miss it and end up in a weird state.
(byebug) Dir.pwd
"/Users/richiethomas/Workspace/ThreeEasyPieces/14/gitlab-development-kit"
(byebug) File.exist? './run'
```

This tells me that either `chdir: $gdk_root` doesn't do what I think it does, or we won't always be in the project root directory by the time we hit this `exec` statement.

*UPDATE:* my hypothesis is that the user *might* already be in the root directory, but they might *also* be in a subdirectory of that directory.

---

## The "install" command

```
when 'install'
  exec(MAKE, *ARGV, chdir: $gdk_root)
```

When the user has run `gdk install`, this code dynamically runs either `gmake` or `make` depending on whether the user's OS is BSD or not, and passing to this program any arguments originally passed to `gdk install`.

---

## The "update" command

```
when 'update'
   # Otherwise we would miss it and end up in a weird state.
   puts "\n> Running `make self-update` separately in case the Makefile is updated..\n\n"
   system(MAKE, 'self-update', chdir: $gdk_root)

   puts "\n> Running `make self-update update`..\n\n"
   exec(MAKE, 'self-update', 'update', chdir: $gdk_root)
```

This branch handles the scenario when `gdk update` is run.  As the comment states, `make self-update` is first run by itself, in case the Makefile itself has been updated.  Assuming we're not running on BSD (and that our build process will be `make`), we first run `make self-update` by itself.  This executes the `self-update` task (in our project's Makefile), which looks like this:

### The "self-update" Makefile task:

```
self-update: unlock-dependency-installers
	@echo ""
	@echo "--------------------------"
	@echo "Running self-update on GDK"
	@echo "--------------------------"
	@echo ""
	cd ${gitlab_development_root} && \
		git stash && \
		git checkout master && \
		git fetch && \
		support/self-update-git-worktree
```

The job of `make self-update` is to first run `unlock-dependency-installers`, which removes a few dotfiles (specifically `.gitlab-bundle`, `.gitlab-shell-bundle`, `.gitlab-yarn`, and `.gettext`).  It then gets the local Git branch into a clean and up-to-date status, and finally runs the `self-update-git-worktree` bash script, located inside the project's `/support` directory:

### The "self-update-git-worktree" script:

```
#!/usr/bin/env ruby

def run(cmd)
  puts "(support/self-update-git-worktree) > #{cmd.join(' ')}"
  system(*cmd)
end

ci_project_dir = ENV['CI_PROJECT_DIR']
ci_sha = ENV['CI_COMMIT_SHA']

# We need to retrieve the commit SHA if the source project is a fork
if ci_project_dir && ci_sha
  run(%W[git remote add source #{ci_project_dir}])
  run(%W[git fetch source #{ci_sha}])
end

if ci_sha
  run(%W[git checkout #{ci_sha}])
else
  run(%w[git merge --ff-only origin/master])
end
```

This script is heavily dependent on two environment variables, named `CI_COMMIT_SHA` and `CI_PROJECT_DIR`, and I'm not sure where these variables are set (or why).  But from what I can see, if *both* of these variables are set, we add a remote repository named 'source', and set it equal to the value of our `CI_PROJECT_DIR` envar.  This implies that the value of this envar is equal to a git repo URL.

We then fetch the code from that remote up to the commit that `CI_COMMIT_SHA` references.  Then, we check if `CI_PROJECT_DIR` exists, and if it does, we check out that commit.  Otherwise, we fast-forward our local master to the current value at the `origin/master` remote repo.

Once the `self-update-git-worktree` script is finished running, we run `make self-update` a 2nd time (not sure why), followed by `make update`:

### "make update"

```
# Update gitlab, gitlab-shell, gitlab-workhorse, gitlab-pages and gitaly
# Pull gitlab directory first since dependencies are linked from there.
update: ensure-postgres-running unlock-dependency-installers gitlab/.git/pull gitlab-shell-update gitlab-workhorse-update gitlab-pages-update gitaly-update gitlab-update gitlab-elasticsearch-indexer-update
```

This is the heart of the `gdk update` command, since this is what pulls down and sets up the latest versions of all the various Gitlab projects.  Someday I'll have to do a whole series of posts on what each of these various Make commands do.

---

## The "diff-config" command

```
when 'diff-config'
require_relative './config_diff.rb'

   files = %w[
     gitlab/config/gitlab.yml
     gitlab/config/database.yml
     gitlab/config/unicorn.rb
     gitlab/config/puma.rb
     gitlab/config/resque.yml
     gitlab-shell/config.yml
     gitlab-shell/.gitlab_shell_secret
     redis/redis.conf
     .ruby-version
     Procfile
     gitlab-workhorse/config.toml
     gitaly/config.toml
     nginx/conf/nginx.conf
   ]

   file_diffs = files.map do |file|
     ConfigDiff.new(file)
   end

   file_diffs.each do |diff|
     $stderr.puts diff.make_output
   end

   file_diffs.each do |diff|
     puts diff.output unless diff.output == ""
   end

   true
```

Defines the `gdk diff-config` command, and requires a file named `config_diff.rb`.  This file implements a `ConfigDiff` class.  One thing to call out is that the reason why the `require` statement is included here (and not at the top of the file) is likely to load the logic lazily, so the interpreter only spends the time to load the file if it's needed (i.e. if the user does in fact call `gdk diff-config`).

The tricky part here is what's included in the `ConfigDiff` class:

<details>

<summary>Click to see the code</summary>

```
require 'fileutils'

class ConfigDiff
  attr_reader :file, :output, :make_output

  def initialize(file)
    @file = file

    execute
  end

  def file_path
    @file_path ||= File.join($gdk_root, file)
  end

  private

  def execute
    FileUtils.mv(file_path, "#{file_path}.unchanged")

    @make_output = update_config_file

    @output = diff_with_unchanged
  ensure
    File.rename("#{file_path}.unchanged", file_path)
  end

  def update_config_file
    run(GDK::MAKE, file)
  end

  def diff_with_unchanged
    run('git', 'diff', '--no-index', '--color', "#{file}.unchanged", file)
  end

  def run(*commands)
    IO.popen(commands.join(' '), chdir: $gdk_root, &:read).chomp
  end
end
```

</details>

This class's constructor takes a filename as a string, renames it to "#{filename}.unchanged", builds a new version of the file using Make, and does a diff between just-renamed version of the file and the just-built version.  Finally, it restores the user's version as the only existing copy of the file.

The `gdk diff-config` command performs this same operation on a list of files, printing the Make-built version of each file to $stderr, and printing the diff (if there is one) to $stdout.

---

## The "reconfigure" command

```
when 'reconfigure'
   remember!($gdk_root)
   exec(MAKE, 'touch-examples', 'unlock-dependency-installers', 'postgresql-sensible-defaults', 'all', chdir: $gdk_root)
```

The `gdk reconfigure` command writes the `$gdk_root` directory to the `.gdk-install-root` file, then runs the `touch-examples`, `unlock-dependency-installers`, `postgresql-sensible-defaults`, and `all` Make commands.

---

## The "psql" command

```
when 'psql'
   exec('psql', '-h', File.join($gdk_root, 'postgresql'), '-p', pg_port.to_s, *ARGV, chdir: $gdk_root)
```

The "psql" command connects to Gitlab's database via the Postgres CLI interface.

---

## The "redis-cli" command

```
when 'redis-cli'
  exec('redis-cli', '-s', File.join($gdk_root, 'redis/redis.socket'), *ARGV, chdir: $gdk_root)
```

The "redis-cli" command starts a Redis connection using a socket located in the project's directory.  I *believe* this socket file is created by Redis when GDK starts the redis server.  What I'm not sure about is how it knows to put the socket file in GDK's `/redis` folder.  It's possible that one of Redis's assumptions is that there will be a `/redis` folder relative to the path in which you start the server, and that GDK ensures the server is always started from the project root folder.  That's my best guess.

---

## The "env" command

I hope you're ready for a rabbit hole, because that's what we've got here.  Buckle up.

```
when 'env'
  GDK::Env.exec(ARGV)
```

Running `gdk env` invokes the `exec` method of the GDK::Env module, which is located in `lib/gdk/env.rb`.  The `exec` method looks like:

```
module GDK
  module Env
    class << self
      def exec(argv)
        if argv.empty?
          print_env
          exit
        else
          exec_env(argv)
        end
      end
```

### If no arguments were passed to `gdk env`:

In this case, the module's `print_env` method is called:

#### The "print_env" method:

```
def print_env
  env.each do |k, v|
    puts "export #{Shellwords.shellescape(k)}=#{Shellwords.shellescape(v)}"
  end
end
```

This takes each of the items in the collection returned by the `env` method, and puts it to the screen in way that's safe to display in a shell environment.

#### The "env" and "get_project" methods:

```
def env
  case get_project
  when 'gitaly'
    { 'GOPATH' => File.join($gdk_root, 'gitaly') }
  when 'gitlab-workhorse'
    { 'GOPATH' => File.join($gdk_root, 'gitlab-workhorse') }
  when 'gitlab-shell', 'go-gitlab-shell'
    { 'GOPATH' => File.join($gdk_root, 'go-gitlab-shell') }
  else
    {}
  end
end

def get_project
  relative_path = Pathname.new(Dir.pwd).relative_path_from(Pathname.new($gdk_root)).to_s
  relative_path.split('/').first
end
```

The return value of `get_project` is the first-generation subfolder inside of `$gdk_root` from which the `gdk env` command is run.  For example, if I run `gdk env` from within the `gitlab-workhorse/foo/bar/baz` subfolder, `get_project` splits this subfolder on the `/` character into an array (`['gitlab-workhorse', 'foo', 'bar', 'baz']`), and returns the first item in that array (`'gitlab-workhorse'`).

The return value of `env` depends on which subfolder `get_project` returns.  The 4 subfolders it cares about are `gitaly`, `gitlab-workhorse`, `gitlab-shell`, and `go-gitlab-shell`.  If it's one of these 4 folders, it returns a certain key-value pair (which gets printed out by `print_env`).  Otherwise it returns an empty hash.  This key-value pair corresponds to an environment variable name (in all cases this name is `GOPATH`), along with its value.

---

### If any arguments *were* passed to `gdk env`:

In this case, the module's `exec_env` method is called:

```
def exec_env(argv)
  # Use Kernel:: namespace to avoid recursive method call
  Kernel::exec(env, *argv)
end
```

Those arguments are passed to the Ruby Kernel module's `exec` method, again depending on which subfolder the command was run from.  If the subfolder corresponds to one of the 4 matching cases, an envar named `GOPATH` is set.  Otherwise, no envars are set.

I'm guessing that the reason this `GOPATH` environment variable needs to be set in the case of these four directories is because these four sub-projects are probably Golang projects, and have a dependency on this envar being set in order to run properly.

---

## The "start", "stop", "restart", and "status" commands

```
when 'start', 'stop', 'restart', 'status'
  assert_supervisor_runit!
  Runit.sv(subcommand, ARGV)
```

When the user runs `gdk start`, `gdk stop`, `gdk restart`, or `gdk status`, Ruby first checks to make sure that Runit (a process manager similar to Foreman) is being used.  That's because these tasks are Runit-specific.  It performs this check by calling the `assert_supervisor_runit!` method:

```
def self.assert_supervisor_runit!
  return if SUPERVISOR == :runit

  abort "this subcommand is only available when using runit"
end
```

This method triggers the `abort` command unless the `SUPERVISOR` constant is set to `:runit`, which happens when the `GDK_RUNIT` is set to 1.

This makes sense because the next command (`Runit.sv(subcommand, ARGV)`), is Runit-specific.

This is the point in my research at which I started to feel a bit out of my depth.  I Googled around a bit for info on Runit, and quickly found that it's related to running processes on a Unix-like OS (MacOS, BSD, Solaris).  In particular, I found (this article)[https://www.mikeperham.com/2014/07/07/use-runit/] by Sidekiq creator Mike Perham, which says the following:

> The point of an init system is to start and supervise processes when the machine boots up. If you're building a modern
> web site, you want memcached, redis, postgresql, mysql and other daemons to start up immediately when the machine boots.
> Supervision means the init system will restart the process immediately if it disappears for some reason, e.g. a crash.
> Reliability of the init system is paramount so simplicity is a key attribute.

So to summarize, Runit is an init system for Unix-like operating systems, which means it helps start certain mission-critical programs when the OS boots up (and restarts them if they crash).  Makes sense.

---

I'm going to skip describing in detail how the `Runit` module works, and therefore skip describing the workings of `gdk` commands which depend on Runit (such as `gdk start/stop/restart/status/tail`), because describing this module will sidetrack me from my overall goal of learning how `gdk run` works.  Suffice to say that my educated guess is that all these commands relate to running GDK using the experimental Runit process manager instead of the default Foreman manager.

UPDATE- this is confirmed from this Youtube video published by the Gitlab core team: https://www.youtube.com/watch?v=njukxpVM1L0&feature=youtu.be

---

## The "help" command, and everything

```
 when 'help'
   puts File.read(File.join($gdk_root, 'HELP'))
   true
 else
   puts "Usage: #{PROGNAME} run|init|install|update|reconfigure|psql|redis-cli|diff-config|version|help [ARGS...]"
   false
 end
```

These are the last two branches of the case statement in this module, and we'll breeze through their logic fairly quickly since they're quite simple:

```
when 'help'
  puts File.read(File.join($gdk_root, 'HELP'))
  true
```

This branch is executed when the user types `gdk help`.  It simply prints the contents of the `HELP` file (found in the project's root directory) to the console, and exits with a 0 exit code.

```
 else
   puts "Usage: #{PROGNAME} run|init|install|update|reconfigure|psql|redis-cli|diff-config|version|help [ARGS...]"
   false
 end
```

This means that if the user enters any other command besides the ones recognized by the case statement (i.e. `gdk foo` or some other unrecognized command), Ruby will print out a list of recognized commands, and exit with a non-zero exit code.

---

## The End!

That's it for the GDK module!  Here are some unanswered questions I thought of along the way, which I may or may not come back to when I re-read and edit this post:

---

# Unanswered questions:

1. When (and why) are the `.gitlab-bundle`, `.gitlab-shell-bundle`, `.gitlab-yarn`, and `.gettext` files created?  (these are mentioned in the `unlock-dependency-installers` task in the project's Makefile)

2. In the `update` branch of the `gdk.rb` case statement, `system` is run first, and `exec` is run next.  What's the difference between these two Ruby commands?

Also, the printed statement says "Running `make self-update`...", but this won't always be the case.  The build command that's run on a BSD installation is `gmake`, not `make`.  Our printed user feedback could be more accurate in this edge case if we string-interpolated this value, like so:

```
  puts "\n> Running `#{MAKE} self-update` separately in case the Makefile is updated..\n\n"
```

3. Why is `Dir.chdir` called (here)[https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/lib/runit.rb#L38], and again (here)[https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/lib/runit.rb#L7] in the method called immediately after?

4. What's the reason we pass `chdir: $gdk_root` to some calls to `exec`?

5. In the `gdk update` task, after we've called `make self-update` a first time, why do we call it a 2nd time?